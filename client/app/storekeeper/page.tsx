import type { NextPage } from "next";
import { Content } from "@/components/storekeeper/home/content";

const Home: NextPage = () => {
  return <Content />;
};

export default Home;
